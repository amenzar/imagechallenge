package com.mercadolibre;

import javax.imageio.ImageIO;
import java.awt.*;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

public class Main {

    public static class Pice {
        BufferedImage img;
        Integer upRight;
        Integer upLeft;
        Integer downRight;
        Integer downLeft;
    }

    public static void main(String[] args) throws IOException {

        // Parse

        File file = new File("/Users/egutierrez/Downloads/challenge.png");
        FileInputStream fis = new FileInputStream(file);
        BufferedImage image = ImageIO.read(fis);

        int rows = 20;
        int cols = 20;

        int chunkWidth = image.getWidth() / cols;
        int chunkHeight = image.getHeight() / rows;

        Pice imgs[][] = new Pice[cols][rows];

        Map<Integer, Integer> rgbCount = new HashMap<>();
        for (int x = 0; x < rows; x++) {
            for (int y = 0; y < cols; y++) {
                Pice pice = new Pice();
                pice.img = new BufferedImage(chunkWidth, chunkHeight, image.getType());

                draw(image, pice.img, chunkWidth, chunkHeight, x, y);

                pice.upLeft = pice.img.getRGB(chunkHeight - 5, 5);
                pice.upRight = pice.img.getRGB(chunkHeight - 5, chunkWidth - 5) ;
                pice.downLeft = pice.img.getRGB(5, 5);
                pice.downRight = pice.img.getRGB(chunkWidth - 5, 5);

                rgbCount.putIfAbsent(pice.upLeft, 0);
                rgbCount.putIfAbsent(pice.upRight, 0);
                rgbCount.putIfAbsent(pice.downLeft, 0);
                rgbCount.putIfAbsent(pice.downRight, 0);

                rgbCount.put(pice.upLeft, rgbCount.get(pice.upLeft) + 1);
                rgbCount.put(pice.upRight, rgbCount.get(pice.upRight) + 1);
                rgbCount.put(pice.downLeft, rgbCount.get(pice.downLeft) + 1);
                rgbCount.put(pice.downRight, rgbCount.get(pice.downRight) + 1);

                imgs[x][y] = pice;

            }
        }

        System.out.println(rgbCount);

        System.out.println("Splitting done");

        // Sort

        // Print

        BufferedImage result = new BufferedImage(image.getWidth(), image.getHeight(), image.getType());
        for (int x = 0; x < rows; x++) {
            for (int y = 0; y < cols; y++) {
                draw2(imgs[x][y].img, result, chunkWidth, chunkHeight, x, y);
            }
        }

        File outputfile = new File("/Users/egutierrez/Downloads/challenge_result.png");
        ImageIO.write(result, "png", outputfile);

    }

    public static void draw2(BufferedImage image, BufferedImage chunk, int chunkWidth, int chunkHeight, int x, int y) throws IOException {
        Graphics2D gr = chunk.createGraphics();
        gr.drawImage(image,
                chunkWidth * y, chunkHeight * x,
                chunkWidth * y + chunkWidth, chunkHeight * x + chunkHeight,
                0, 0,
                chunkWidth, chunkHeight,
                null);
        gr.dispose();
    }

    public static void draw(BufferedImage image, BufferedImage chunk, int chunkWidth, int chunkHeight, int x, int y) throws IOException {
        Graphics2D gr = chunk.createGraphics();
        gr.drawImage(image,
                0, 0,
                chunkWidth, chunkHeight,
                chunkWidth * y, chunkHeight * x,
                chunkWidth * y + chunkWidth,
                chunkHeight * x + chunkHeight, null);
        gr.dispose();

        File outputfile = new File("/Users/egutierrez/Downloads/challenge_" + x + "_" + y + ".png");
        ImageIO.write(chunk, "png", outputfile);
    }

    private static boolean matchesLeftBorder(Pice leftPiece, Pice myPiece) {
        return leftPiece.upRight == myPiece.upLeft && leftPiece.downRight == myPiece.downLeft
    }

    private static boolean matchesRightBorder(Pice rightPiece, Pice myPiece) {
        return rightPiece.upLeft == myPiece.upRight && rightPiece.downLeft == myPiece.downRight
    }

    private static boolean matchesUpperBorder(Pice upperPiece, Pice myPiece) {
        return upperPiece.downLeft == myPiece.upLeft && upperPiece.downRight == myPiece.upRight
    }

    private static boolean matchesLowerBorder(Pice lowerPiece, Pice myPiece) {
        return lowerPiece.upLeft == myPiece.downLeft && lowerPiece.upRight == myPiece.downRight
    }

    private static boolean matchesUpperLeftCorner(Pice myPiece, Pice leftPiece, Pice upperPiece) {
        return matchesUpperBorder(upperPiece, myPiece) && matchesLeftBorder(leftPiece, myPiece)
    }

    private static boolean matchesUpperRightCorner(Pice myPiece, Pice rightPiece, Pice upperPiece) {
        return matchesUpperBorder(upperPiece, myPiece) && matchesRightBorder(rightPiece, myPiece)
    }

    private static boolean matchesLowerLeftCorner(Pice myPiece, Pice leftPiece, Pice upperPiece) {
        return matchesUpperBorder(upperPiece, myPiece) && matchesLeftBorder(leftPiece, myPiece)
    }

    private static boolean matchesLowerRightCorner(Pice myPiece, Pice rightPiece, Pice lowerPiece) {
        return matchesLowerBorder(lowerPiece, myPiece) && matchesRightBorder(rightPiece, myPiece)
    }

}
